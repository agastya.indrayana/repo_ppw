from django.shortcuts import render


def index(request):
    return render(request, 'lab3/index.html')


def about(request):
    return render(request, 'lab3/about.html')


def contact(request):
    return render(request, 'lab3/contact.html')


def registration(request):
    return render(request, 'lab3/registration.html')


def product(request):
    return render(request, 'lab3/product.html')
