from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
    path('registration/', views.registration, name='registration'),
    path('schedule/', views.schedule, name='schedule'),
    path('delete_all/', views.delete_all, name='delete_all'),
]
