from django import forms
from lab5.models import Activity

class TaskForm(forms.Form):
    your_name = forms.CharField(label='Your name',required=False, max_length=100)
    your_email = forms.EmailField(label='your email',required=False)
    date_of_birth = forms.DateField(label='your birthday')

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['activity_name','location','category','date','time']
        widgets = {
            'activity_name': forms.TextInput(attrs={'class' : 'form-control'}),
            'location': forms.TextInput(attrs={'class' : 'form-control'}),
            'category': forms.TextInput(attrs={'class' : 'form-control'}),
            'date': forms.DateInput(attrs={'type':'date', 'class' : 'form-control'}),
            'time' :forms.TimeInput(attrs={'type':'time', 'class' : 'form-control'})
        }
        labels = {
            "activity_name": "Activity Name",
            "location": "Location",
            "category" : "Category",
            "date" : "Date",
            "time" : "Time",
        }
