from django.db import models

class Activity(models.Model):
    activity_name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()

    class meta:
        ordering = ['date','time']
