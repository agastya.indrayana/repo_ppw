from django.shortcuts import render, redirect
from lab5.models import Activity
from lab5.forms import ActivityForm
from datetime import date, time

def index(request):
    upcomingActivities = Activity.objects.all().values().order_by('date','time')
    # upcomingActivities.order_by('date').order_by('time')
    # pastActivities = Activity.objects.filter(date__lt=now).order_by('-date')
    # pastActivities.order_by('-date').order_by('-time')

    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            activity_name = form.cleaned_data['activity_name']
            location = form.cleaned_data['location']
            category = form.cleaned_data['category']
            date = form.cleaned_data['date']
            time = form.cleaned_data['time']

            form.save()

            form = ActivityForm()
            context = {'upcomingActivities' : upcomingActivities, 'ActivityForm': form,'activity_name': activity_name,'location':location,
            'category' : category, 'date':date, 'time': time}

            return render(request, 'lab5/index.html', context)
    else:
        form = ActivityForm()
        context = {'upcomingActivities' : upcomingActivities, 'ActivityForm': form}

    return render(request, 'lab5/index.html', context)

def registration(request):
    return render(request, 'lab5/registration.html')

def schedule(request):
    upcomingActivities = Activity.objects.all().values().order_by('date')

    return render(request, 'lab5/schedulingDisplay.html', {"upcomingActivities":upcomingActivities})

def delete_all(request):
    Activity.objects.all().delete()
    return redirect('/index/')
